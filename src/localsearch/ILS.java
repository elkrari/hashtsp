package localsearch;

import neighborhood.Neighborhood;
import tsp.Instance;
import tsp.Solution;
import utils.Movement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ThreadLocalRandom;


public class ILS {
	
	public static Solution ILS(Instance tsp, Solution s, float nbIter, float nbPert, float maxTries, String strategy){
		int n=tsp.getDimension(),it=0, itBest=0, nbTries=0;
		float perturbationStrength;
		Solution lo = s.copy(),previous=s.copy(), best = s.copy();
		ArrayList<Long> history = new ArrayList<>();
		TwoOpt opt = new localsearch.TwoOpt();
		System.out.print(best.getFitness()+","+it);
		for (it=1;it<=n*nbIter;it++){
			if(strategy.equals("best"))
				opt.bestFit(lo,tsp);
			else
				opt.SLS(lo,tsp);
			if(lo.getHash()==previous.getHash())
				perturbationStrength=0.3f;
			else if (history.contains(lo.getHash()))
				perturbationStrength=0.2f;
			else{
				addToHistory(history,0,lo.getHash());// history.add(lo.getHash());
				perturbationStrength=0.1f;
			}
			if(best.getFitness()>lo.getFitness()) {
				best = lo.copy();
				itBest=it;
				nbTries=0;
				perturbationStrength=0f;
				System.out.print(";"+best.getFitness()+","+it);
			}else if(++nbTries>=n*maxTries){
				nbTries=0;
				perturbationStrength=2f;
			}
			previous = lo.copy();
			lo = perturbation(tsp,lo,new neighborhood.TwoOpt(),perturbationStrength);
		}
		//System.out.print(best.getFitness()+","+itBest);
		System.out.print("\t"+history.size());
		return best;
	}
	
	private static Solution perturbation(Instance tsp, Solution solution, Neighborhood neighborhood, float nbPert){
		int instanceSize = tsp.getDimension();
		for(int i=0;i<instanceSize*nbPert;i++)
			solution = neighborhood.move(tsp, solution,new Movement());
		return solution;
	}
	
	private static void addToHistory(ArrayList<Long> history, int size, long hash){
		if(size>0 && history.size()==size)
			history.remove(0);
		history.add(hash);
	}
	
	
}
