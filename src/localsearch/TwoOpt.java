/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package localsearch;

import tsp.*;
import utils.Movement;

import java.util.ArrayList;

/**
 *
 * @author elkrari
 */
public class TwoOpt implements LocalSearch{
    
    @Override
    public Movement bestNeighbour(Solution s, Instance tsp) {
        neighborhood.TwoOpt opt = new neighborhood.TwoOpt();
        int i,j, u=0, v=0, n = tsp.getDimension();
        int del, deltaMin = 0;
        for (i = 0; i <= n - 2; i++) {
            for (j = i + 2; (i == 0 && j < n - 1) || (i > 0 && j < n); j++) {
                del = opt.fitnessDelta(tsp, s, new Movement(i,j) );
                if (del < deltaMin) {
                    u = i;
                    v = j;
                    deltaMin = del;
                }
            }
        }
        if (u != 0 || v != 0) {
            Movement mvt = new Movement(u, v);
            opt.move(tsp, s, mvt);
            return mvt;
        }
        else
            return null;
    }
    
    @Override
    public Movement firstNeighbour(Solution s, Instance tsp) {
        neighborhood.TwoOpt opt = new neighborhood.TwoOpt();
        int i,j=0, n = tsp.getDimension();
        boolean found=false;
        outerLoop:
        for (i = 0; i <= n - 2; i++) {
            for (j = i + 2; (i == 0 && j < n - 1) || (i > 0 && j < n); j++) {
                if( opt.fitnessDelta(tsp, s, new Movement(i,j) )<0) {
                    found = true;
                    break outerLoop;
                }
            }
        }
        if (found) {
            Movement mvt = new Movement(i, j);
            opt.move(tsp, s, mvt);
            return mvt;
        }
    
        return null;
    }
    
    public Movement firstRandomNeighbour(Solution s, Instance tsp) {
        neighborhood.TwoOpt opt = new neighborhood.TwoOpt();
        int r,i,j, n = tsp.getDimension();
        ArrayList<Movement> mvt = new ArrayList<>();
        Movement tmp;
        for (i = 0; i <= n - 2; i++)
            for (j = i + 2; (i == 0 && j < n - 1) || (i > 0 && j < n); j++)
                mvt.add(new Movement(i,j));
        
        int size = mvt.size();
        for(i=0;i<size;i++){
            java.util.Random rand = new java.util.Random();
            r = rand.nextInt((size-1 - i) + 1) + i;
            if( opt.fitnessDelta(tsp, s, mvt.get(r) )<0) {
                opt.move(tsp, s, mvt.get(r));
                return mvt.get(r);
            }else{
                tmp = mvt.get(i);
                mvt.set(i,mvt.get(r));
                mvt.set(r,tmp);
            }
        }
        return null;
    }
    
    @Override
    public boolean bestFit(Solution s, Instance tsp){
        boolean found = false;
        while (bestNeighbour(s,tsp)!=null)
            found = true;
        return found;
    }
    
    @Override
    public ArrayList<Movement> bestFit(Solution s, Instance tsp, ArrayList<Movement> history) {
        Movement mvt;
        while ((mvt=bestNeighbour(s,tsp))!=null)
            history.add(mvt);
        return history;
    }
    
    public ArrayList<Solution> bestFitH(Solution s, Instance tsp){
        ArrayList<Solution> hist = new ArrayList<>();
        while (bestNeighbour(s,tsp)!=null)
            hist.add(s.copy());
        return hist;
    }
    
    @Override
    public boolean firstFit(Solution s, Instance tsp){
        boolean found = false;
        while (firstNeighbour(s,tsp)!=null)
            found = true;
        return found;
    }
    
    public boolean SLS(Solution s, Instance tsp){
        boolean found = false;
        while (firstRandomNeighbour(s,tsp)!=null)
            found = true;
        return found;
    }
    
    @Override
    public ArrayList<Movement> firstFit(Solution s, Instance tsp, ArrayList<Movement> history) {
        Movement mvt;
        while ((mvt=firstNeighbour(s,tsp))!=null)
            history.add(mvt);
        return history;
    }
    
    public ArrayList<Solution> firstFitH(Solution s, Instance tsp){
        ArrayList<Solution> hist = new ArrayList<>();
        while (firstNeighbour(s,tsp)!=null)
            hist.add(s.copy());
        return hist;
    }
    
}
