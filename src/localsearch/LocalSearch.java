package localsearch;

import tsp.*;
import utils.Movement;

import java.util.ArrayList;

public interface LocalSearch {
    
    /**
     * Find the best-improving neighbour of a Solution {@code s}
     * @param s Current {@code Solution} to be improved
     * @param tsp The TSP {@code Instance}
     * @return the {@code Movement} who led to the new Solution or {@code null} if {@code s} is a local optimum.
     */
    Movement bestNeighbour(Solution s, Instance tsp);
    
    /**
     * Find the first improving neighbour of a Solution {@code s} with a deterministic approach
     * @param s Current {@code Solution} to be improved
     * @param tsp The TSP {@code Instance}
     * @return the {@code Movement} who led to the new Solution or {@code null} if {@code s} is a local optimum.
     */
    Movement firstNeighbour(Solution s, Instance tsp);

    /**
     * Local search with "best fit" descent
     * strategy: choosing the best improving
     * neighbor on each step, also known as "Hill Climbing"
     * @param s Current {@code Solution} to be improved
     * @param tsp The TSP {@code Instance}
     * @return {@code true} if s has moved to a local optimum or {@code false} if s is already a local optimum.
     */
    boolean bestFit(Solution s, Instance tsp);
    
    /**
     * Local search with "best fit" descent
     * strategy: choosing the best improving
     * neighbor on each step, also known as "Hill Climbing"
     * @param s Current {@code Solution} to be improved
     * @param tsp The TSP {@code Instance}
     * @param history The history of previous movements (if any)
     * @return The complete history of movements based on {@code history}
     */
    ArrayList<Movement> bestFit(Solution s, Instance tsp, ArrayList<Movement> history);
    

    /**
     * Local search with "first fit" descent
     * strategy: choosing the first improving
     * neighbor on each step
     * @param s Current {@code Solution} to be improved
     * @param tsp The TSP {@code Instance}
     * @return A local optimum
     */
    boolean firstFit(Solution s, Instance tsp);
    
    /**
     * Local search with "first fit" descent
     * strategy: choosing the first improving
     * neighbor on each step
     * @param s Current {@code Solution} to be improved
     * @param tsp The TSP {@code Instance}
     * @param history The history of previous movements (if any)
     * @return The complete history of movements based on {@code history}
     */
    ArrayList<Movement> firstFit(Solution s, Instance tsp, ArrayList<Movement> history);
    
    
}
