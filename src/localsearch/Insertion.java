/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package localsearch;

import tsp.Instance;
import tsp.Solution;
import utils.Movement;

import java.util.ArrayList;

/**
 *
 * @author elkrari
 */
public class Insertion implements LocalSearch{
    
    @Override
    public Movement bestNeighbour(Solution s, Instance tsp) {
        neighborhood.Insertion ins = new neighborhood.Insertion();
        int i,j, u=0, v=0, n = tsp.getDimension();
        int del, deltaMin = 0;
        for (i = 0; i < n-1 ; i++) {
            for (j = i + 1; (i == 0 && j < n - 1) || (i > 0 && j < n); j++) {
                del = ins.fitnessDelta(tsp, s, new Movement(i,j) );
                if (del < deltaMin) {
                    u = i;
                    v = j;
                    deltaMin = del;
                }
            }
        }
        if (u != 0 || v != 0) {
            Movement mvt = new Movement(u, v);
            ins.move(tsp, s, mvt);
            return mvt;
        }
        return null;
    }
    
    @Override
    public Movement firstNeighbour(Solution s, Instance tsp) {
        neighborhood.Insertion ins = new neighborhood.Insertion();
        int i,j=0, n = tsp.getDimension();
        boolean found = false;
        outerLoop:
        for (i = 0; i < n - 1; i++) {
            for (j = i + 1; (i == 0 && j < n - 1) || (i > 0 && j < n); j++) {
                if (ins.fitnessDelta(tsp, s, new Movement(i,j) ) < 0) {
                    found = true;
                    break outerLoop;
                }
            }
        }
        if (found) {
            Movement mvt = new Movement(i,j);
            ins.move(tsp, s, mvt);
            return mvt;
        }
        return null;
    }
    
    @Override
    public boolean bestFit(Solution s, Instance tsp){
        boolean found = false;
        while (bestNeighbour(s,tsp)!=null)
            found = true;
        return found;
    }
    
    @Override
    public ArrayList<Movement> bestFit(Solution s, Instance tsp, ArrayList<Movement> history) {
        Movement mvt;
        while ((mvt=bestNeighbour(s,tsp))!=null)
            history.add(mvt);
        return history;
    }
    
    @Override
    public boolean firstFit(Solution s, Instance tsp){
        boolean found = false;
        while (firstNeighbour(s,tsp)!=null)
            found = true;
        return found;
    }
    
    @Override
    public ArrayList<Movement> firstFit(Solution s, Instance tsp, ArrayList<Movement> history) {
        Movement mvt;
        while ((mvt=firstNeighbour(s,tsp))!=null)
            history.add(mvt);
        return history;
    }
    
}
