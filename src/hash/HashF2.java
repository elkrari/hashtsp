/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hash;

import tsp.Instance;
import tsp.Solution;

import static utils.Maths.mod;

/**
 *
 * @author elkrari
 */
public class HashF2 implements HashFunction {
    
    @Override
    public long subHash(Solution s, Instance tsp, int i, int j){
        int[] t = s.getTour();
        long dist=tsp.getDistances()[t[j]][t[i]];
        return  mod(t[j],t[i])*mod(dist,t[i]+t[j])*mod(t[i]*t[j],t[i]+t[j])*dist ;
        //return  Math.multiplyExact(Math.multiplyExact(Math.multiplyExact(mod(t[j],t[i]),Math.addExact(fit%t[i],fit%t[j])),Math.addExact(t[j],t[i])),dist) ;
    }

    @Override
    public long hash(Solution S, Instance tsp){
        int n=S.getTour().length;
        long hash = 0;
        
        for(int i=0;i<n-1;i++)
            hash = Math.addExact(hash,subHash(S,tsp,i,i+1));

         hash = Math.addExact(hash,subHash(S,tsp,0,n-1));
         
         return hash;
    }
}
