/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hash;

import tsp.Instance;
import tsp.Solution;

/**
 * David L. Woodruff and Eitan Zemel. 1993.
 * Hashing vectors for tabu search.
 * Annals of Operations Research41 (1993), 123–137
 * @author elkrari
 */
public class HashH1 implements HashFunction {
    
    @Override
    public long subHash(Solution s, Instance tsp, int i, int j){
        int[] t = s.getTour();
        return (Math.multiplyExact(tsp.getRand()[i],t[i]))%(Long.MAX_VALUE+1) ;
    }

    @Override
    public long hash(Solution S, Instance tsp){
        int n=S.getTour().length;
        long hash = 0;
        
        for(int i=0;i<n-1;i++)
            hash = Math.addExact(hash,subHash(S,tsp,i,i+1));

         hash = Math.addExact(hash,subHash(S,tsp,0,n-1));
         
         return hash;
    }
}
