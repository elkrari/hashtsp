/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hash;

import tsp.Instance;
import tsp.Solution;

/**
 * Toffolo Túlio A.M., Vidal Thibaut, and Wauters Tony. 2019.
 * Heuristics for vehiclerouting problems: Sequence or set optimization?
 * Computers Operations Research105 (2019), 118 – 131.
 * @author elkrari
 */
public class HashHp implements HashFunction {
    
    @Override
    public long subHash(Solution s, Instance tsp, int i, int j){
        int[] t = s.getTour();
        return Math.multiplyExact(tsp.getRho()[i],t[j]) ;
    }

    @Override
    public long hash(Solution S, Instance tsp){
        int n=S.getTour().length;
        long hash = 0;
        
        for(int i=0;i<n-1;i++)
            hash = Math.addExact(hash,subHash(S,tsp,i,i+1));

         hash = Math.addExact(hash,subHash(S,tsp,0,n-1));
         
         return hash;
    }
}
