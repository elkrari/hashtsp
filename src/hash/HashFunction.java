package hash;

import tsp.Instance;
import tsp.Solution;

public interface HashFunction {
	
	/**
	 * Compute the hash value of the edge composed of
	 * the ith and the jth nodes of the TSP tour
	 *
	 * @param s The {@code Solution} to whom the edge to be hashed is belonging
	 * @param tsp The {@code Instance} to whom <code>s</code> is belonging
	 * @param i The ith node (or city) of the tour of <code>s</code>
	 * @param j The jth node (or city) of the tour of <code>s</code>
	 * @return The hash value of the permutation edge (<code>i</code>,<code>j</code>)
	 */
	long subHash(Solution s, Instance tsp, int i, int j);
	
	/**
	 * Compute the hash value of a TSP {@code Solution}
	 *
	 * @param s The {@code Solution} to be hashed
	 * @param tsp The {@code Instance} to whom <code>s</code> is belonging
	 * @return The hash value of the {@code Solution}
	 */
	long hash(Solution s, Instance tsp);
	
	
}
