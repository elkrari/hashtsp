/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hash;

import tsp.Instance;
import tsp.Solution;

import static utils.Maths.mod;

/**
 *
 * @author elkrari
 */
public class HashF1 implements HashFunction {
    
    @Override
    public long subHash(Solution s, Instance tsp, int i, int j){
        int[] t = s.getTour();
        long dist=tsp.getDistances()[t[j]][t[i]];
        //return mod(t[j],t[i])*(t[i]*t[j])*(t[j]+t[i])*dist ;
        return Math.multiplyExact(mod(t[j],t[i]),Math.multiplyExact(t[i],Math.multiplyExact(t[j],Math.multiplyExact((t[j]+t[i]),dist))));
    }

    @Override
    public long hash(Solution S, Instance tsp){
        int n=S.getTour().length;
        long hash = 0;
        
        for(int i=0;i<n-1;i++)
            hash = Math.addExact(hash,subHash(S,tsp,i,i+1));

         hash = Math.addExact(hash,subHash(S,tsp,0,n-1));
         
         return hash;
    }
}
