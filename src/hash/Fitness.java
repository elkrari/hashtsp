package hash;

import tsp.Instance;
import tsp.Solution;

public class Fitness implements HashFunction {
	@Override
	public long subHash(Solution s, Instance tsp, int i, int j) {
		return tsp.getDistances()[s.getTour()[j]][s.getTour()[i]];
	}
	
	@Override
	public long hash(Solution s, Instance tsp) {
		int n=s.getTour().length;
		long hash = 0;
		
		for(int i=0;i<n-1;i++)
			hash = Math.addExact(hash,subHash(s,tsp,i,i+1));
		
		hash = Math.addExact(hash,subHash(s,tsp,0,n-1));
		
		return hash;
	}
}
