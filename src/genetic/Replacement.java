package genetic;

import java.util.ArrayList;

public class Replacement {
	public Population generational(Population init, ArrayList<Chromosome> offspring){
		init.empty();
		for(Chromosome ch:offspring)
			init.add(ch);
		
		return init;
	}
	
	public Population elitist(Population population, ArrayList<Chromosome> offspring){
		int initSize=population.getChromosomes().size(),size=initSize;
		for(Chromosome ch:offspring) {
			population.add(ch);
			size++;
		}
		population.sort();
		while(size!=initSize){
			population.remove(-1+size--);
		}
		return population;
	}
}
