package genetic;

import tsp.Solution;

public class Chromosome implements Comparable {
	
	private tsp.Solution solution;
	private int evaluationNbr;
	
	public Chromosome(Solution solution, int evaluationNbr) {
		this.solution = solution;
		this.evaluationNbr = evaluationNbr;
	}
	public Chromosome(Solution solution) {
		this.solution = solution;
		this.evaluationNbr = 0;
	}
	
	public Solution getSolution() {
		return solution;
	}
	
	public void setSolution(Solution solution) {
		this.solution = solution;
	}
	
	public int getEvaluationNbr() {
		return evaluationNbr;
	}
	
	public void setEvaluationNbr(int evaluationNbr) {
		this.evaluationNbr = evaluationNbr;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Chromosome)) return false;
		Chromosome that = (Chromosome) o;
		return solution.equals(that.solution);
	}
	
	@Override
	public int hashCode() {
		return 0;
	}
	
	@Override
	public String toString() {
		return "evaluationNbr=" + evaluationNbr +
				", solution=" + solution ;
	}
	
	@Override
	public int compareTo(Object o) {
		Chromosome chromosome = (Chromosome) o;
		return this.getSolution().compareTo(chromosome.getSolution());
	}
	
	public int compareTo2(Object o) {
		Chromosome chromosome = (Chromosome) o;
		return this.getEvaluationNbr() - chromosome.getEvaluationNbr();
	}
	
}
