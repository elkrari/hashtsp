package genetic;

import tsp.Instance;
import tsp.Solution;

import java.util.ArrayList;

public class Selection {
	
	public ArrayList<Chromosome> tournament(Instance tsp, Population pop, int size){
		assert size%2 == 0;
		ArrayList<Chromosome> candidates = new ArrayList<>();
		Population sample;
		Chromosome best;
		while(candidates.size()<size){
			sample = pop.getRandomSample(size);
			best = sample.getBest();
				candidates.add(best);
		}
		return candidates;
	}
}
