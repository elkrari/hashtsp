package genetic;

import hash.HashF1;
import hash.HashFunction;
import tsp.Instance;
import tsp.Solution;

import java.util.ArrayList;
import java.util.HashMap;

public class Population {
	
	private ArrayList<Chromosome> chromosomes;
	
	
	public Population(ArrayList<Chromosome> chromosomes) {
		this.chromosomes = chromosomes;
	}
	
	public Population(Instance tsp, int size, HashFunction h){
		chromosomes = new ArrayList<>();
		for(int i=0;i<size;i++)
			addRandom(tsp,h,i+1);
			//chromosomes.add(new Chromosome(new Solution(tsp,new NearestNeighbor().newTour(tsp), h), i+1));
	}
	
	public Population(Instance tsp){
		new Population(tsp,tsp.getDimension(),new HashF1());
	}
	
	public Population() {
	}
	
	public ArrayList<Chromosome> getChromosomes() {
		return chromosomes;
	}
	
	public void setChromosomes(ArrayList<Chromosome> chromosomes) {
		this.chromosomes = chromosomes;
	}
	
	public boolean contains(Chromosome s){
		for(Chromosome ss:this.chromosomes)
			if(s.equals(ss))
				return true;
		return false;
	}
	
	public void add(Chromosome s){
		chromosomes.add(s);
	}
	
	public void addRandom(Instance tsp, HashFunction h, int numEval){
		chromosomes.add(new Chromosome(new Solution(tsp,h), numEval));
	}
	
	public void remove(int index){
		assert index< chromosomes.size();
		chromosomes.remove(index);
	}
	
	public void sort(){
		chromosomes.sort(Chromosome::compareTo);
	}
	
	public Chromosome getBest(){
		Chromosome best = chromosomes.get(0);
		for(Chromosome ss:this.chromosomes)
			if(ss.getSolution().getFitness()<best.getSolution().getFitness())
				best=ss;
		return best;
	}
	
	public void empty(){
		chromosomes.clear();
	}
	
	public Population getRandomSample(int size){
		ArrayList<Chromosome> sample = new ArrayList<>();
		Chromosome tmp;
		int i,r;
		for(i=0;i<size;i++){
			java.util.Random rand = new java.util.Random();
			r = rand.nextInt((this.chromosomes.size()-1 - i) + 1) + i;
			sample.add(chromosomes.get(r));
			tmp = chromosomes.get(i);
			chromosomes.set(i, chromosomes.get(r));
			chromosomes.set(r,tmp);
		}
		return new Population(sample);
	}
	
	public float convergenceRateHash(){
		HashMap<Long,Integer> map = new HashMap<>();
		for(Chromosome ss:this.chromosomes)
			map.putIfAbsent(ss.getSolution().getHash(),1);
		return 100f*(1f+(-map.size()+1)/(float)this.chromosomes.size());
	}
	public float convergenceRateFitness(){
		HashMap<Integer,Integer> map = new HashMap<>();
		for(Chromosome ss:this.chromosomes)
			map.putIfAbsent(ss.getSolution().getFitness(),1);
		return 100f*(1f+(-map.size()+1)/(float)this.chromosomes.size());
	}
	
	public Chromosome getLastEvaluated(){
		Chromosome last = chromosomes.get(0);
		int maxEval = 0;
		for(Chromosome ss: chromosomes)
			if(maxEval<ss.getEvaluationNbr()){
				maxEval = ss.getEvaluationNbr();
				last = ss;
			}
		return last;
	}
	
	public int getLastEvalNbr(){
		int maxEval = 0;
		for(Chromosome ss: chromosomes)
			if(maxEval<ss.getEvaluationNbr()){
				maxEval = ss.getEvaluationNbr();
			}
		return maxEval;
	}
	
	public void purgeOlder(int nbr){
		chromosomes.sort(Chromosome::compareTo);
		Chromosome ch = chromosomes.get(0);
		chromosomes.sort(Chromosome::compareTo2);
		int i=0;
		for(int x=0;x<nbr;x++){
			if(chromosomes.get(i)==ch){
				x--;
				i++;
			}else
				chromosomes.remove(i);
		}
	}
	
	public Population copy(){
		Population p = new Population();
		p.chromosomes = new ArrayList<>();
		for(Chromosome s:this.chromosomes)
			p.add(s);
		return p;
	}
	
	@Override
	public String toString() {
		String p="";
		for(Chromosome s: chromosomes)
			p = p.concat(s.toString().concat("\n"));
		
		return p;
	}
}
