package genetic;

import neighborhood.*;
import tsp.Instance;
import tsp.Solution;

public class Mutation {
	public Solution insertion(Instance tsp, Solution s){
		return new Insertion().move(tsp,s);
	}
	
	public Solution swap(Instance tsp, Solution s){
		return new Swap().move(tsp,s);
	}
	
	public Solution twoOpt(Instance tsp, Solution s){
		return new TwoOpt().move(tsp,s);
	}
}
