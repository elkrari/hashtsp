package genetic;

import hash.HashFunction;
import localsearch.LocalSearch;
import tsp.Instance;

import java.util.ArrayList;

public class Memetic {
	
	private int evals;
	
	public Memetic(int evals) {
		this.evals = evals;
	}
	public Memetic() {
		this.evals = 0;
	}
	
	public int getEvals() {
		return evals;
	}
	
	public void setEvals(int evals) {
		this.evals = evals;
	}
	
	public void memetic_convergenceRate(Instance tsp, Population population, HashFunction h,
	                                    int sizeSelection, float convergenceRateLimit, LocalSearch ls){
		
		Selection selection = new Selection();
		Crossover crossover = new Crossover();
		Replacement replacement = new Replacement();
		int maxTries = tsp.getDimension(), nbTries;
		ArrayList<Chromosome> selected, offspring = new ArrayList<>();
		Chromosome[] off;
		nbTries=0;
		for (Chromosome s : population.getChromosomes())
		    if(ls.bestFit(s.getSolution(), tsp))
			    s.setEvaluationNbr(++evals);
		while(population.convergenceRateHash()<convergenceRateLimit && nbTries<maxTries){//for (int k = 0; k < nbrGenerations; k++) {
			//System.out.print(population.convergenceRateHash() + ";");
			selected = selection.tournament(tsp, population, sizeSelection);
			for (int t = 0; t < sizeSelection/2; t++) {
				if (!selected.get(t * 2).equals(selected.get(t * 2 + 1))) {
					off = crossover.onePoint(tsp, selected.get(t * 2), selected.get(t * 2 + 1),h);
					if(ls.bestFit(off[0].getSolution(), tsp))
						evals++;
					if(ls.bestFit(off[1].getSolution(), tsp))
						evals++;
					off[0].setEvaluationNbr(++evals); offspring.add(off[0]);
					off[1].setEvaluationNbr(++evals); offspring.add(off[1]);
				}else
					nbTries++;
			}
			population = replacement.elitist(population, offspring);
			offspring.clear();
		}
	}
	
	public void memetic_convergenceRate_evaluations(Instance tsp, Population population, HashFunction h,
	                                                int sizeSelection, float convergenceRateLimit, int maxEvaluations, LocalSearch ls){
		
		Selection selection = new Selection();
		Crossover crossover = new Crossover();
		Replacement replacement = new Replacement();
		int maxTries = tsp.getDimension()*2, nbTries;
		ArrayList<Chromosome> selected, offspring = new ArrayList<>();
		Chromosome[] off;
		nbTries=0;
		for (Chromosome s : population.getChromosomes())
			if(ls.bestFit(s.getSolution(), tsp))
				s.setEvaluationNbr(++evals);
		while(population.convergenceRateHash()<convergenceRateLimit && nbTries<maxTries && evals<maxEvaluations){
			//System.out.print(population.convergenceRateHash() + ";");
			selected = selection.tournament(tsp, population, sizeSelection);
			for (int t = 0; t < sizeSelection/2; t++) {
				if (!selected.get(t * 2).equals(selected.get(t * 2 + 1))) {
					off = crossover.onePoint(tsp, selected.get(t * 2), selected.get(t * 2 + 1),h);
					if(ls.bestFit(off[0].getSolution(), tsp))
						evals++;
					if(ls.bestFit(off[1].getSolution(), tsp))
						evals++;
					off[0].setEvaluationNbr(++evals); offspring.add(off[0]);
					off[1].setEvaluationNbr(++evals); offspring.add(off[1]);
				}else
					nbTries++;
			}
			population = replacement.elitist(population, offspring);
			offspring.clear();
		}
	}
	
	public void memetic_maxGenerations(Instance tsp, Population population, HashFunction h,
	                                   int sizeSelection, int maxNbrGenerations, LocalSearch ls){
		
		Selection selection = new Selection();
		Crossover crossover = new Crossover();
		Replacement replacement = new Replacement();
		ArrayList<Chromosome> selected, offspring = new ArrayList<>();
		Chromosome[] off;
		for (Chromosome s : population.getChromosomes())
			if(ls.bestFit(s.getSolution(), tsp))
				s.setEvaluationNbr(++evals);
		for (int k = 0; k < maxNbrGenerations; k++) {
			selected = selection.tournament(tsp, population, sizeSelection);
			for (int t = 0; t < sizeSelection/2; t++) {
				if (!selected.get(t * 2).equals(selected.get(t * 2 + 1))) {
					off = crossover.onePoint(tsp, selected.get(t * 2), selected.get(t * 2 + 1),h);
					if(ls.bestFit(off[0].getSolution(), tsp))
						evals++;
					if(ls.bestFit(off[1].getSolution(), tsp))
						evals++;
					off[0].setEvaluationNbr(++evals); offspring.add(off[0]);
					off[1].setEvaluationNbr(++evals); offspring.add(off[1]);
				}
			}
			population = replacement.elitist(population, offspring);
			offspring.clear();
		}
	}
	
	public void memetic_maxGenerations_evaluations(Instance tsp, Population population, HashFunction h,
	                                               int sizeSelection, int maxNbrGenerations, int maxEvaluations, LocalSearch ls){
		
		Selection selection = new Selection();
		Crossover crossover = new Crossover();
		Replacement replacement = new Replacement();
		ArrayList<Chromosome> selected, offspring = new ArrayList<>();
		Chromosome[] off;
		for (Chromosome s : population.getChromosomes())
			if(ls.bestFit(s.getSolution(), tsp))
				s.setEvaluationNbr(++evals);
		for (int k = 0; k < maxNbrGenerations && evals<maxEvaluations; k++) {
			selected = selection.tournament(tsp, population, sizeSelection);
			for (int t = 0; t < sizeSelection/2; t++) {
				if (!selected.get(t * 2).equals(selected.get(t * 2 + 1))) {
					off = crossover.onePoint(tsp, selected.get(t * 2), selected.get(t * 2 + 1),h);
					if(ls.bestFit(off[0].getSolution(), tsp))
						evals++;
					if(ls.bestFit(off[1].getSolution(), tsp))
						evals++;
					off[0].setEvaluationNbr(++evals); offspring.add(off[0]);
					off[1].setEvaluationNbr(++evals); offspring.add(off[1]);
				}
			}
			population = replacement.elitist(population, offspring);
			offspring.clear();
		}
	}
}
