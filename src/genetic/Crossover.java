package genetic;

import hash.HashFunction;
import tsp.Instance;
import tsp.Solution;

public class Crossover {
	public Chromosome[] onePoint(Instance tsp, Chromosome p1, Chromosome p2, HashFunction h){
		Chromosome[] child = new Chromosome[2];
		Chromosome c1,c2;
		int     n = p1.getSolution().getTour().length,
				point = (int)(Math.random()*n);
		int[] permC1 = new int[n],permC2 = new int[n];
		boolean[] inC1 = new boolean[n+1],inC2 = new boolean[n+1];
		for(int i=0;i<=point;i++){
			permC1[i]=p1.getSolution().getTour()[i];
			inC1[p1.getSolution().getTour()[i]] = true;
			permC2[i]=p2.getSolution().getTour()[i];
			inC2[p2.getSolution().getTour()[i]] = true;
		}
		int k1 = point+1,k2 = point+1;
		for(int i=0;i<n;i++){
			if(!inC1[p2.getSolution().getTour()[i]])
				permC1[k1++]=p2.getSolution().getTour()[i];
			if(!inC2[p1.getSolution().getTour()[i]])
				permC2[k2++]=p1.getSolution().getTour()[i];
		}
		c1 = new Chromosome(new Solution(tsp,permC1,h) );
		c2 = new Chromosome(new Solution(tsp,permC2,h) );
		child[0]=c1;
		child[1]=c2;
		return child;
	}
}
