package constructive;

/**
 *
 * @author elkrari
 */


import tsp.Instance;

public class NearestInsertion implements Constructive {

    /**
     * Build a new tour following the nearest insertion
     * strategy with a random starting node/city
     * @param tsp An instance of the problem
     * @return A new tour
     */
    @Override
    public int[] newTour(Instance tsp) {
        int length = tsp.getDimension();
        int tour[] = new int[length];
        /*Initializing the permutation/tour with 1,2,3,...,n */
        for(int k=0;k<length;k++){
            tour[k]=k+1;
        }
        int i=0;
        int Vi,n,temp,dist;
        /*Replacing the 1st and 2nd cities with random ones
        * Keeping the cities 1 and 2 will result in the same final solution*/
        Vi=(int) (Math.random() * length);
        temp = tour[0]; tour[0] = tour[Vi]; tour[Vi] = temp;
        Vi=(int) (Math.random() * length - 1) + 1;
        temp = tour[1]; tour[1] = tour[Vi]; tour[Vi] = temp;
        /*Select at each iteration a random city (c) from those not yet chosen
        * Inserting c to its nearest city in the constructed sub-tour*/
        for (i=2; i<length; i++) {
            Vi=(int) (Math.random() * length - i) + i;
            temp = tour[i]; tour[i] = tour[Vi]; tour[Vi] = temp;
            n=0;
            dist = tsp.distance(tour[0],tour[Vi]);
            for(int j=1;j<=i;j++)
                if(dist>tsp.distance(tour[j],tour[Vi]))
                    n=j;
            tour=shift(tour,n,i);
        }
        return tour;
    }


    private int[] shift(int[] tour, int start, int end){
        int temp = tour[end];
        for(int i=end;i>start;i--){
            tour[i]=tour[i-1];
        }
        tour[start]=temp;
        return tour;
    }
}
