package constructive;

/**
 *
 * @author elkrari
 */

import tsp.Instance;

public interface Constructive {

    int[] newTour(Instance tsp);
}
