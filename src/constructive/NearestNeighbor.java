package constructive;

/**
 *
 * @author elkrari
 */


import tsp.Instance;

public class NearestNeighbor implements Constructive {

    /**
     * Build a new tour following the nearest neighbor
     * strategy with a random starting node/city
     * @param tsp An instance of the problem
     * @return A new tour
     */
    @Override
    public int[] newTour(Instance tsp) {
        java.util.Random rand = new java.util.Random();
        int r = rand.nextInt(tsp.getDimension());

        return newTour(tsp,r);
    }

    /**
     * Build a new tour following the nearest neighbor
     * strategy with a predefined starting city
     * @param tsp An instance of the problem
     * @param start The chosen starting city
     * @return A new tour
     */
    public int[] newTour(Instance tsp, int start) {
        int length = tsp.getDimension();
        int[] S = new int[length];

        for(int i=1;i<=length;i++)
            S[i-1]=i;

        int s;
        java.util.Random rand = new java.util.Random();
        int r = rand.nextInt(length);
        s = S[0];
        S[0] = S[start];
        S[start] = s;

        int nearest;
        for(int i=0;i<length-1;i++){
            nearest = nearestCity(tsp,S,i+1,S[i]);
            s = S[i];
            S[i] = S[nearest];
            S[nearest] = s;
        }

        return S;
    }

    /**
     * Look for the nearest node to {@code city} in the sub-tour
     * starting from {@code bound} to the end.
     * @param tsp An instance of the problem
     * @param tour The current tour
     * @param bound Starting index of not yet chosen cities
     * @param city The city to be connected to its new neighbor
     * @return The index of the nearest node to {@code city}
     */
    private int nearestCity(Instance tsp,  int[] tour, int bound, int city){
        int nearest = bound;
        for(int i=bound+1;i<tour.length;i++)
            if(tsp.distance(tour[i], city)<tsp.distance(tour[nearest],city))
                nearest = i;
        return nearest;
    }
}
