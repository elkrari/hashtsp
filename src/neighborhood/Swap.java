package neighborhood;

import hash.HashFunction;
import tsp.Instance;
import tsp.Solution;
import utils.Movement;

public class Swap implements Neighborhood {
    @Override
    public Solution move(Instance tsp, Solution s, Movement mvt) {
        int i = mvt.getNodes()[0], j = mvt.getNodes()[1];
        s.setFitness(s.getFitness()+fitnessDelta(tsp, s, mvt));
        s.setHash(s.getHash()+hashDelta(tsp,s,mvt));
        int k = s.getTour()[i];

        int x = s.getPos()[k];
        s.getPos()[s.getTour()[i]] = s.getPos()[s.getTour()[j]];
        s.getPos()[s.getTour()[j]] = x;

        s.getTour()[i] = s.getTour()[j];
        s.getTour()[j] = k;

        return s;
    }
    
    @Override
    public Solution move(Instance tsp, Solution s) {
        int i,j,n=tsp.getDimension();
        i = (int)(Math.random()*n-1);
        do{
            j = (int)(Math.random()*(n-i))+i;
        }while (i==j);
        return move(tsp,s,new Movement(i,j));
    }
    
    @Override
    public Integer fitnessDelta(Instance tsp, Solution s, Movement mvt) {
        int n = tsp.getDimension(), i = mvt.getNodes()[0], j = mvt.getNodes()[1];
        int[] t = s.getTour();
        int[][] dist = tsp.getDistances();
        
        if(i==0 && j==(n-1))
            return  dist[t[0]][t[n-2]] + dist[t[n-1]][t[1]]
                    - dist[t[0]][t[1]] - dist[t[n-1]][t[n-2]];
        
        final int i_1 = ((i - 1) % n + n) % n;
        final int j1 = ((j + 1) % n + n) % n;
        if((j%n+n)%n==((i+1)%n+n)%n || (j%n+n)%n== i_1)
            return dist[t[i_1]][t[j]] + dist[t[i]][t[j1]]
                    - dist[t[i_1]][t[i]] - dist[t[j]][t[j1]];
        
        return dist[t[i_1]][t[j]] + dist[t[j]][t[i+1]] + dist[t[j-1]][t[i]] + dist[t[i]][t[j1]]
                - dist[t[i_1]][t[i]] - dist[t[i]][t[i+1]] - dist[t[j-1]][t[j]] - dist[t[j]][t[j1]];
    }
    
    @Override
    public long hashDelta(Instance tsp, Solution s, Movement mvt) {
        int n = tsp.getDimension(), i = mvt.getNodes()[0], j = mvt.getNodes()[1];
        HashFunction h = s.getHashFunction();
        if(i==0 && j==(n-1))
            return  h.subHash(s,tsp,0,n-2)+h.subHash(s,tsp, n-1, 1)
                    - h.subHash(s,tsp,0,1)-h.subHash(s,tsp, n-1, n-2);
    
        final int i_1 = ((i - 1) % n + n) % n;
        final int j1 = ((j + 1) % n + n) % n;
        if((j%n+n)%n==((i+1)%n+n)%n || (j%n+n)%n== i_1)
            return h.subHash(s,tsp,i_1,j)+h.subHash(s,tsp, i, j1)
                    - h.subHash(s,tsp,i_1,i)-h.subHash(s,tsp, j, j1);
        
        return h.subHash(s,tsp,i_1,j)+h.subHash(s,tsp, j, i+1)+h.subHash(s,tsp, j-1, i)+h.subHash(s,tsp, i, j1)
                -h.subHash(s,tsp, i_1, i)-h.subHash(s,tsp, i, i+1)-h.subHash(s,tsp, j-1, j)-h.subHash(s,tsp, j, j1);
    }
}
