package neighborhood;

import hash.HashFunction;
import tsp.*;
import utils.Movement;

public class TwoOpt implements Neighborhood {


    @Override
     public Solution move(Instance tsp, Solution s, Movement mvt) {
        if (mvt.getNodes().length<2)
            mvt = randomMVT(s.getTour().length);
        int i=mvt.getNodes()[0]+1, j=mvt.getNodes()[1];
        if(j<i){
            int k = j;
            j=i;
            i=k;
        }
        Swap sw = new Swap();
        while (i < j) {
            s = sw.move(tsp, s, new Movement(i,j));
            i++;
            j--;
        }
        return s;
    }
    
    @Override
    public Solution move(Instance tsp, Solution s) {
        int i,j,n=tsp.getDimension();
        i = (int)(Math.random()*n-1);
        do{
            j = (int)(Math.random()*(n-i+2))+i+2;
        }while ((i == 0 && j >= n - 1) || (i > 0 && j >= n));
        return move(tsp,s,new Movement(i,j));
    }

    @Override
    public Integer fitnessDelta(Instance tsp, Solution s, Movement mvt) {
        int i=mvt.getNodes()[0], j=mvt.getNodes()[1];
        int n= tsp.getDimension();
        int[] t = s.getTour();
        final int i1 = ((i + 1) % n + n) % n;
        final int j1 = ((j + 1) % n + n) % n;
        return tsp.distance(t[i],t[j])
                +tsp.distance(t[i1],t[j1])
                -tsp.distance(t[i],t[i1])
                -tsp.distance(t[j],t[j1]);
    }
    
    @Override
    public long hashDelta(Instance tsp, Solution s, Movement mvt) {
        int i=mvt.getNodes()[0], j=mvt.getNodes()[1], n=s.getTour().length;
        HashFunction h = s.getHashFunction();
    
        final int i1 = ((i + 1) % n + n) % n;
        final int j1 = ((j + 1) % n + n) % n;
        return h.subHash(s,tsp,i,j)+h.subHash(s,tsp, i1, j1)
                -h.subHash(s,tsp,i, i1)-h.subHash(s,tsp,j, j1);
    }
    
    private Movement randomMVT(int n){
        int i,j;
        i = (int) (Math.random()*n-1);
        j=i;
        while(j>i-2 && j<i+2)
            j = (int) (Math.random()*n);
        
        return new Movement(i,j);
    }

}
