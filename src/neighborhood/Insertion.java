package neighborhood;

import hash.HashFunction;
import tsp.Instance;
import tsp.Solution;
import utils.Movement;

public class Insertion implements Neighborhood {
    @Override
    public Solution move(Instance tsp, Solution s, Movement mvt) {
        int i = mvt.getNodes()[0], j = mvt.getNodes()[1];
        int x = s.getTour()[i];// y = S.getPos()[j];
        s.setFitness(s.getFitness()+fitnessDelta(tsp, s, mvt));
        s.setHash(s.getHash()+hashDelta(tsp,s,mvt));

        if(i<j) {
            for (int k = i; k < j; k++) {
                s.getTour()[k] = s.getTour()[k + 1];
                s.getPos()[s.getTour()[k]]--;
            }
            s.getTour()[j] = x;
            s.getPos()[x] = j;
        }else{
            for (int k = i; k > j; k--) {
                s.getTour()[k] = s.getTour()[k - 1];
                s.getPos()[s.getTour()[k]]++;
            }
            s.getTour()[j] = x;
            s.getPos()[x] = j;
        }
        return s;
    }
    
    @Override
    public Solution move(Instance tsp, Solution s) {
        int i,j,n=tsp.getDimension();
        i = (int)(Math.random()*n-1);
        do{
            j = (int)(Math.random()*(n-i))+i;
        }while ((i == 0 && j >= n - 1) || (i > 0 && j >= n));
        return move(tsp,s,new Movement(i,j));
    }
    
    @Override
    public Integer fitnessDelta(Instance tsp, Solution s, Movement mvt) {
        int i = mvt.getNodes()[0], j = mvt.getNodes()[1];
        int n = tsp.getDimension();
        int[] t = s.getTour();
    
        final int i1 = ((i + 1) % n + n) % n;
        final int j1 = ((j + 1) % n + n) % n;
        final int i_1 = ((i - 1) % n + n) % n;
        return tsp.distance(t[i_1],t[i1])
                + tsp.distance(t[i],t[j])
                + tsp.distance(t[i],t[j1])
                - tsp.distance(t[i_1],t[i])
                - tsp.distance(t[i],t[i1])
                - tsp.distance(t[j],t[j1]);
    }
    
    @Override
    public long hashDelta(Instance tsp, Solution s, Movement mvt) {
        int i = mvt.getNodes()[0], j = mvt.getNodes()[1], n= s.getTour().length;
        HashFunction h = s.getHashFunction();
    
        final int i1 = ((i + 1) % n + n) % n;
        final int j1 = ((j + 1) % n + n) % n;
        final int i_1 = ((i - 1) % n + n) % n;
        return h.subHash(s,tsp, i_1,(i1))+h.subHash(s,tsp,i,j)+h.subHash(s,tsp,i,(j1))
                -h.subHash(s,tsp, i_1,i)-h.subHash(s,tsp, i,i1)-h.subHash(s,tsp, j,j1);
    }
}
