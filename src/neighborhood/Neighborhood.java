package neighborhood;

import tsp.*;
import utils.Movement;

public interface Neighborhood {

    /**
     * Perform on a Solution a movement
     * following a neighborhood function
     * (Insertion; Swap; 2-Opt)
     * @param tsp A TSP instance
     * @param s Solution on which the movement
     *          will be applied
     * @param mvt The movement to apply
     * @return A neighbor solution of s
     */
    Solution move(Instance tsp, Solution s, Movement mvt);
    
    /**
     * Perform on a Solution a random movement
     * following a neighborhood function
     * (Insertion; Swap; 2-Opt)
     * @param tsp A TSP instance
     * @param s Solution on which the movement
     *          will be applied
     * @return A neighbor solution of s
     */
    Solution move(Instance tsp, Solution s);

    /**
     * Compute the fitness difference between
     * the current solution (s) and its neighbor
     * obtained by applying {@code mvt}.
     * Instead of computing the new fitness linearly,
     * this function allow to get it in a constant time
     * @param tsp A TSP instance
     * @param s The current solution
     * @param mvt The movement to be applied
     * @return A fitness difference
     */
    Integer fitnessDelta(Instance tsp, Solution s, Movement mvt);
    
    /**
     * //TODO
     * @param tsp
     * @param s
     * @param mvt
     * @return
     */
    long hashDelta(Instance tsp, Solution s, Movement mvt);


}
