package neighborhood;

import tsp.Instance;
import tsp.Solution;
import utils.Movement;

//TODO
class DoubleBridgeMove implements Neighborhood {
    @Override
    public Solution move(Instance tsp, Solution s, Movement mvt) {
        if(mvt.getNodes().length<4)
            mvt = randomMVT(s.getTour().length);

        return null;
    }
    
    @Override
    public Solution move(Instance tsp, Solution s) {
        return null;
    }

    /*public Solution doubleBridgeMove(Solution S, int range, int radius, boolean dlbFlag){
        Solution solDBM ;
        range=Maths.abs(range);
        int i,j,g,h,x,y,n=this.dim;
        ArrayList<Integer> posT=new ArrayList<Integer>();
        int[] db=new int[n];
        solDBM = new Pair<int[], Integer>(db,0);
        i = (int) (Maths.random()*n);
        if(range==0){
            j=i;
            while(j>i-2 && j<i+2)
                j = (int) (Maths.random()*n);
            g=j;
            while((g>i-2 && g<i+2) || (g>j-2 && g<j+2))
                g = (int) (Maths.random()*n);
            h=g;
            while((h>i-2 && h<i+2) || (h>j-2 && h<j+2) || (h>g-2 && h<g+2))
                h = (int) (Maths.random()*n);
        }else{
            if(range>n)
                range=n;
            j=i;
            while(j>i-2 && j<i+2)
                j = (int) (Maths.random()*range+i)%n;
            g=j;
            while((g>i-2 && g<i+2) || (g>j-2 && g<j+2))
                g = (int) (Maths.random()*range+i)%n;
            h=g;
            while((h>i-2 && h<i+2) || (h>j-2 && h<j+2) || (h>g-2 && h<g+2))
                h = (int) (Maths.random()*range+i)%n;
        }

        posT.add(i);posT.add(j);posT.add(g);posT.add(h);
        Collections.sort(posT);
        if(dlbFlag){
            i=(((posT.get(0)-radius+1)%n)+n)%n;
            j=(((posT.get(1)-radius+1)%n)+n)%n;
            g=(((posT.get(2)-radius+1)%n)+n)%n;
            h=(((posT.get(3)-radius+1)%n)+n)%n;
            for(x=0;x<2*radius;x++){
                i++;
                j++;
                g++;
                h++;
            }
        }


        int deltaDBM = -this.MatDis[S.getValue0()[(i)]-1][S.getValue0()[(i+1)]-1]
                -this.MatDis[S.getValue0()[j]-1][S.getValue0()[(j+1)]-1]
                -this.MatDis[S.getValue0()[g]-1][S.getValue0()[(g+1)]-1]
                -this.MatDis[S.getValue0()[h]-1][S.getValue0()[((((h+1)%n)+n)%n)]-1]
                +this.MatDis[S.getValue0()[i]-1][S.getValue0()[(g+1)]-1]
                +this.MatDis[S.getValue0()[h]-1][S.getValue0()[(j+1)]-1]
                +this.MatDis[S.getValue0()[g]-1][S.getValue0()[(i+1)]-1]
                +this.MatDis[S.getValue0()[j]-1][S.getValue0()[((((h+1)%n)+n)%n)]-1];
        solDBM=solDBM.setAt0(db);
        solDBM=solDBM.setAt1(S.getValue1()+deltaDBM);
        return solDBM;
    }*/

    public Movement randomMVT(int n){
        int i,j,x,y;
        i = (int) (Math.random()*n);

        j=i;
        while(j>i-2 && j<i+2)
            j = (int) (Math.random()*n);
        x=j;
        while((x>i-2 && x<i+2) || (x>j-2 && x<j+2))
            x = (int) (Math.random()*n);
        y=x;
        while((y>i-2 && y<i+2) || (y>j-2 && y<j+2) || (y>x-2 && y<x+2))
            y = (int) (Math.random()*n);

        return new Movement(i,j,x,y);
    }

    @Override
    public Integer fitnessDelta(Instance tsp, Solution s, Movement mvt) {
        return null;
    }
    
    @Override
    public long hashDelta(Instance tsp, Solution s, Movement mvt) {
        return 0;
    }
    
}
