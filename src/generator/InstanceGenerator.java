package generator;

/**
 *
 * @author elkrari
 */


import tsp.City;
import tsp.Instance;

import java.util.ArrayList;

import static tsp.City.*;

public class InstanceGenerator {

    /**
     * Generate a new TSP instance with randomly
     * distributed nodes over the geographic space
     * delimited by {@code width} and {@code height}
     * @param dimension Instance size (number of cities)
     * @param edgeWeightType Edge Weight Type: Euclidean or Geographic
     * @param width The width of geographic space
     * @param height The height of geographic space
     * @return A new instance randomly generated
     */
    public Instance generate(int dimension, String edgeWeightType, int width, int height){
        Instance tsp = new Instance();

        tsp.setDimension(dimension);
        tsp.setEdgeWeightType(edgeWeightType);

        //-----Generate TSP Coordinates-----
        ArrayList<City> coordinates = new ArrayList<>();
        City cc;
        for(int i=0;i<dimension;i++){
            do{
                cc = new City((float)Math.random() * width,(float)Math.random() * height);
            }while(i!=0 && exist(coordinates,cc,i-1));
            coordinates.add(cc);
        }
        tsp.setCities(coordinates);

        int[][] distances = new int[dimension+1][dimension+1];
        if("EUC_2D".equals(edgeWeightType))
            for (int i = 1; i < dimension - 1; i++) {
                for (int j = i + 1; j < dimension; j++) {
                    distances[i][j] = distances[j][i] = distEuc(coordinates.get(i), coordinates.get(j));
                }
            }
        else if(edgeWeightType.equals("GEO"))
            for (int i = 1; i < dimension - 1; i++) {
                for (int j = i + 1; j < dimension; j++) {
                    distances[i][j] = distances[j][i] = distGeo(coordinates.get(i), coordinates.get(j));
                }
            }

        tsp.setDistances(distances);

        return tsp;
    }
    
    public Instance generateFromExisting(Instance i1, int dimension, int fragment){
        assert i1.getDimension()>dimension;
        Instance i2 = new Instance();
        
        i2.setDimension(dimension);
        i2.setEdgeWeightType(i1.getEdgeWeightType());
        
        //-----Get TSP coordinates from the existing instance-----
        ArrayList<City> cities = new ArrayList<>();
        int size=0, start;
        do{
            start = (int)(Math.random()*i1.getDimension());
            for(int i=0;i<fragment;i++){
                if(size<dimension && !exist(cities,i1.getCities().get((start+1)%i1.getDimension()),size-1)) {
                    cities.add(i1.getCities().get((start + 1) % i1.getDimension()));
                    size++;
                }
            }
        }while(cities.size()<dimension);
        
        i2.setCities(cities);
        
        int[][] distances = new int[dimension+1][dimension+1];
        if("EUC_2D".equals(i2.getEdgeWeightType()))
            for (int i = 1; i < dimension; i++) {
                for (int j = i + 1; j <= dimension; j++) {
                    distances[i][j] = distances[j][i] = distEuc(cities.get(i-1), cities.get(j-1));
                }
            }
        else if(i2.getEdgeWeightType().equals("GEO"))
            for (int i = 1; i < dimension; i++) {
                for (int j = i + 1; j <= dimension; j++) {
                    distances[i][j] = distances[j][i] = distGeo(cities.get(i-1), cities.get(j-1));
                }
            }
        i2.setDistances(distances);
        
        return i2;
    }

    private boolean exist(ArrayList<City> setCoordinates, City coordinates, int limit){
        for(int i=0;i<=limit;i++){
            if(setCoordinates.get(i).equals(coordinates))
                return true;
        }
        return false;
    }
}
