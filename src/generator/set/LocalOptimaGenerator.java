package generator.set;

import constructive.Random;
import hash.HashFunction;
import utils.Movement;
import tsp.Instance;
import tsp.Solution;

import java.io.IOException;
import java.util.ArrayList;

import static generator.set.Utils.addIfNotExists;
import static utils.Console.flushPrint;
import static utils.File.getSolutions;

public class LocalOptimaGenerator {
	
	/**
	 * Build a set of distinct local optima (LO) with an ILS algorithm starting with existing LO.
	 * @param tsp The TSP instance to be considered
	 * @param size The (minimal) size of the generated size set
	 * @param file CSV path of existing solutions
	 * @return A set of distinct (local optima) Solutions
	 */
	public static ArrayList<Solution> buildSet(Instance tsp, int size, String file, HashFunction h) throws IOException {
		int n=tsp.getDimension(), t[];
		Solution LO;
		ArrayList<Solution> pop;
		java.io.File csv = new java.io.File(file);
		if (!csv.exists()) {
			csv.createNewFile();
			pop = new ArrayList<>();
		}else{
			//Add existing solutions to the population to not to generate them again
			pop = getSolutions(file,tsp,h);
		}
		while(pop.size()<size) {
			t = new Random().newTour(tsp);
			LO = new Solution(tsp, t);
			new localsearch.TwoOpt().bestFit(LO, tsp);
			pop = addIfNotExists(pop,LO.copy(),file);
			for (int p=1;p<n;p++){
				for(int i=0;i<n/10;i++) //Perturbation based on random 2-Opt move(s)
					LO = new neighborhood.TwoOpt().move(tsp, LO,new Movement());
				new localsearch.TwoOpt().bestFit(LO, tsp);
				pop=addIfNotExists(pop,LO.copy(),file);
			}
			//flushPrint("instance name",pop.size()+"("+100*pop.size()/size+"%)");
		}
		return pop;
	}
}
