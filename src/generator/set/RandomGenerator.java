package generator.set;

import constructive.Random;
import hash.Fitness;
import hash.HashFunction;
import tsp.Instance;
import tsp.Solution;

import java.io.IOException;
import java.util.ArrayList;

import static generator.set.Utils.addIfNotExists;
import static utils.Console.flushPrint;
import static utils.File.getSolutions;

public class RandomGenerator {
	
	/**
	 * Build a set of distinct solutions generated randomly.
	 * @param tsp The TSP instance to be considered
	 * @param size The (minimal) size of the generated size set
	 * @return A set of distinct (random) Solutions
	 */
	public static ArrayList<Solution> buildSet(Instance tsp, int size, String file) throws IOException {
		HashFunction h = new Fitness();
		ArrayList<Solution> pop;
		java.io.File csv = new java.io.File(file);
		if (!csv.exists()) {
			csv.createNewFile();
			pop = new ArrayList<>();
		}else{
			//Add existing solutions to the population to not to generate them again
			pop = getSolutions(file,tsp,h);
		}
		while(pop.size()<size) {
			pop = addIfNotExists(pop, new Solution(tsp, new Random().newTour(tsp),h),file);
			//flushPrint("instance name",pop.size()+"("+100*pop.size()/size+"%)");
		}
		return pop;
	}
	
}
