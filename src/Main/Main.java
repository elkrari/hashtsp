
package Main;

import genetic.Chromosome;
import genetic.Memetic;
import genetic.Population;
import hash.*;
import localsearch.TwoOpt;
import tsp.Instance;
import tsp.Solution;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static utils.Chart.medianOfMedians;
import static utils.File.getSolutions;
import static utils.Maths.average;
import static utils.Similarity.countOccurrences;


/**
 *
 * @author elkrari
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
    	
    	/*Running Example(s)*/
	
	    
	    String instance = "berlin52", path_instance="TSPLIB/", path_solutions="./";
	    //String instance = args[0], path_instance = args[1], path_solutions = args[2];
	    Instance tsp = new Instance(new File(path_instance+instance+".tsp"));
	    int optimalFitness = 426;
	    //int optimalFitness = Integer.parseInt(args[4]);
	    int instance_size = tsp.getDimension();
	    /*-----Instance generator-----*/
	    /*//int size = 200, fragmentSize = 200;
	    int size = Integer.parseInt(args[3]), fragmentSize = Integer.parseInt(args[4]);
	    String name = instance+"_"+size+"_"+fragmentSize;
	    String comment = "Instance generated from TSPLIB/"+instance;
	    String path = path_solutions+name+"_"+ Maths.randChar(5);
	    Instance rand = new InstanceGenerator().generateFromExisting(tsp,size,fragmentSize);
	    //System.out.println(rand.toString());
	    instanceToFile(rand, path, name,comment);*/
	    
	    
	    HashFunction h = new HashH1();
	    /*switch (args[3]){
		    case "Fit":
			    h = new Fitness();
			    break;
		    case "F4":
			    h = new HashF4();
			    break;
		    case "H3":
			    h = new HashH3();
			    break;
		    case "H1":
			    h = new HashH1();
			    break;
		    case "Hp":
			    h = new HashHp();
			    break;
		    default:
			    h = new HashF3();
	    }*/
	    
	    
	    
    	
	    /*-----Building a set of local optima then collision computation-----*/
	    
    	//Local Optima Set Generator
	    /*int set_size = 10*instance_size*instance_size;
	    ArrayList<Solution> set = RandomGenerator.buildSet(tsp,set_size,path_solutions+instance+".csv");*/
	    
	    
	    //Collision Computation
	    /*ArrayList<Solution> localOptima = getSolutions(path_solutions+instance+".csv",tsp,h);
	    //System.out.println("Fitness collisions: "+countFitnessCollision(localOptima));
	    //System.out.println("HashFunction collisions: "+countHashCollision(localOptima));
	    int count = countHashCollision(localOptima), sampleSize = localOptima.size();
	    //float nbPairs = (sampleSize*sampleSize-sampleSize)/2;
	    System.out.println(instance+";"+count);*/
	    
	    //Count distinct values
	    /*HashMap<Long,Integer> map = new HashMap<>();
	    for(Solution s : localOptima)
	    	map.put(s.getHash(),1);
	    System.out.println(map.size());*/
	    
	    //Count redundancies
	    /*ArrayList<Solution> localOptima = getSolutions(path_solutions+instance+".csv",tsp,h);
	    ArrayList<Long> hashes = new ArrayList<>();
	    for(Solution s : localOptima)
	    	hashes.add(s.getHash());
	    long[][] occur = countOccurrences(hashes);
	    System.out.print(instance+";"+occur[0].length+";");
	    for(int f=0;f<occur[0].length;f++)
		    System.out.print(100*(occur[0][f]-optimalFitness)/(float)optimalFitness+";");
	    System.out.print("\n"+instance+";"+occur[0].length+";");
	    for(int f=0;f<occur[0].length;f++)
	        System.out.print(occur[1][f]+";");
	    System.out.println();*/
	
	    
	    /*----- ILS -----*/
	    /*Solution s = new Solution(tsp);
	    Solution s1 = s.copy(), s2 = s.copy();
	    HashFunction h3 = new HashF3();
	    HashFunction fit = new Fitness();
	    s1.setHashFunction(h3); s1.setHash(h3.hash(s1,tsp));
	    s2.setHashFunction(fit); s2.setHash(fit.hash(s2,tsp));
	    System.out.print("Hash:");
	    ILS.ILS(tsp,s1,10,1f, 1f,"best");
	    System.out.print("\nFitness:");
	    ILS.ILS(tsp,s2,10,1f, 1f,"best");
	    System.out.println("");*/
	    
	    /*----- GA / Memetic -----*/
	    /*float convergenceRateLimit = 90;
	    Population population, population1, population2;
	    Memetic memetic = new Memetic();
	    h = new HashF3();
	    population = new Population(tsp, instance_size, h);
	    population1 = population.copy();
	    localsearch.TwoOpt opt = new TwoOpt();
	    System.out.print(instance_size+";");
	    System.out.print(population1.getBest().getSolution().getFitness()+";"+population1.getBest().getEvaluationNbr()+";"+memetic.getEvals());
	    population2 = population.copy();
	    h = new Fitness();
	    for (Chromosome ch : population2.getChromosomes()) {
		    ch.getSolution().setHashFunction(h);
		    ch.getSolution().setHash(h.hash(ch.getSolution(), tsp));
	    }
	    memetic = new Memetic();
	    memetic.memetic_convergenceRate(tsp, population2, h, 2, convergenceRateLimit,opt);
		System.out.println(";"+population2.getBest().getSolution().getFitness()+";"+population2.getBest().getEvaluationNbr()+";"+memetic.getEvals());*/
	
	    
	   
	    
    }
}