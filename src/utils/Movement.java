package utils;

import java.util.Arrays;

/**
 *
 * @author elkrari
 */


public class Movement {
    private int[] nodes;
    private int option;

    public Movement(){
        this.nodes = new int[]{};
    }

    public Movement(int i, int j) {
        this.nodes = new int[]{i,j};
        this.option = 0;
    }

    public Movement(int i, int j, int k) {
        this.nodes = new int[]{i,j,k};
        this.option = 0;
    }

    /*public Movement(int i, int j, int k, int option) {
        this.nodes = new int[]{i,j,k};
        this.option = option;
    }*/

    public Movement(int i, int j, int x, int y) {
        this.nodes = new int[]{i,j,x,y};
        this.option = 0;
    }

    public int[] getNodes() {
        return nodes;
    }

    public void setNodes(int[] nodes) {
        this.nodes = nodes;
    }

    public int getOption() {
        return option;
    }

    public void setOption(int option) {
        this.option = option;
    }
    
    @Override
    public String toString() {
        return "Movement{" +
                "nodes=" + Arrays.toString(nodes) +
                ", option=" + option +
                '}';
    }
}
