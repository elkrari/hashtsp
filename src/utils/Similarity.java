/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package utils;

import tsp.Solution;

import java.util.*;
import java.util.stream.Collectors;

/**
 *
 * @author elkrari
 */
public class Similarity {
    
    /*public boolean areSimilar(Solution S1, Solution S2){
        int[] t1 = S1.getTour(), t2 = S2.getTour();

        if(t1.length != t2.length || S1.getFitness() != S2.getFitness())
            return false;
        
        int shift = 0;
        while(t1[0] != t2[shift])
            shift++;
        
        int i=0, j=shift, count=0, n=t1.length;
        while(count<n && t1[i] == t2[(((j)%n+n)%n)]){
            i++; j++; count++;
        }
        return count==n;
        
    }*/

    public static boolean areSimilar(Solution s1, Solution s2){
        //Hash comparison
        //return s1.getHash()==s2.getHash();
        
        //Permutation comparison
        int l = s1.getTour().length;
        int[] t1 = s1.getTour(), t2 = s2.getTour(), p2 = s2.getPos();
        for(int i=0;i<l;i++)
            if ((!new Edge(t1[i], t1[(i + 1) % l]).equals(new Edge(t2[p2[t1[i]]], t2[((p2[t1[i]] + 1) % l + l) % l])))
                    && (!new Edge(t1[i], t1[(i + 1) % l]).equals(new Edge(t2[p2[t1[i]]], t2[((p2[t1[i]] - 1) % l + l) % l]))))
                return false;
        return  true;
    }

    public static double simRate(Solution s1, Solution s2){
        double k = 0;
        int l = s1.getTour().length;

        int[] t1 = s1.getTour(), t2 = s2.getTour(), p2 = s2.getPos();

        for(int i=0;i<l;i++) {
            if ((new Edge(t1[i], t1[(i + 1) % l]).equals(new Edge(t2[p2[t1[i]]], t2[((p2[t1[i]] + 1) % l + l) % l])))
                    || (new Edge(t1[i], t1[(i + 1) % l]).equals(new Edge(t2[p2[t1[i]]], t2[((p2[t1[i]] - 1) % l + l) % l])))) {
                k++;
            }
        }
        System.out.println("k="+k);
        return  k/l;
    }
    
    public static int countFitnessCollision(ArrayList<Solution> set){
        int count = 0,
                size = set.size();
        
        for(int i=0;i<size-1;i++)
            for(int j=i+1;j<size;j++)
                if (set.get(i).getFitness() == set.get(j).getFitness())
                    count++;
        return count;
    }
    
    public static int countHashCollision(ArrayList<Solution> set){
        int count = 0,
                size = set.size();
        
        for(int i=0;i<size-1;i++)
            for(int j=i+1;j<size;j++)
                if(set.get(i).getHash()==set.get(j).getHash())
                //if(set.get(i).getHash()==set.get(j).getHash() && set.get(i).getFitness()==set.get(j).getFitness())
                    count++;
        return count;
    }
    
    public static long[][] countOccurrences(ArrayList<Long> array){
        //List asList = Arrays.asList(array);
        Set<Long> mySet = new HashSet<Long>(array);
        long[][] occurrences = new long[2][mySet.size()];
        int i = 0;
        for(Long s: mySet){
            //System.out.println(s + ";" +Collections.frequency(array,s));
            occurrences[0][i]=s;
            occurrences[1][i++]=Collections.frequency(array,s);
        }
        return occurrences;
    
    }

    private static class Edge{
        int x;
        int y;

        public Edge(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Edge edge = (Edge) o;

            if ((x == edge.x && y == edge.y) || (x == edge.y && y == edge.x))
                return true;

            return false;
        }

        /*@Override
        public int hashCode() {
            int result = x;
            result = 31 * result + y;
            return result;
        }*/
    }
}
