package utils;

import hash.HashFunction;
import tsp.Instance;
import tsp.Solution;

import java.io.*;
import java.text.Format;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.StringTokenizer;

import static utils.Maths.average;

public class File {

    public static void addSolutionToFile(String path, Solution s){
        BufferedWriter bw = null;
        FileWriter fw = null;

        try {
            java.io.File file = new java.io.File(path);
            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }
            // true = append file
            fw = new FileWriter(file.getAbsoluteFile(), true);
            bw = new BufferedWriter(fw);
            bw.write(s.getFitness()+";"+Arrays.toString(s.getTour()));
            bw.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bw != null) {
                    bw.close();
                }
                if (fw != null) {
                    fw.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public static void addPopulationToFile(String path, ArrayList<Solution> pop){
        BufferedWriter bw = null;
        FileWriter fw = null;

        try {
            java.io.File file = new java.io.File(path);
            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }
            // true = append file
            fw = new FileWriter(file.getAbsoluteFile(), true);
            bw = new BufferedWriter(fw);
            for(Solution s: pop){
                bw.write(s.getFitness()+";"+Arrays.toString(s.getTour()));
                bw.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bw != null) {
                    bw.close();
                }
                if (fw != null) {
                    fw.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public static ArrayList<Integer> getFitnesses(String path){
        java.io.File file = new java.io.File(path);
        String line;
        StringTokenizer st;
        ArrayList<Integer> set = new ArrayList<>();
        try {
            FileInputStream fStream = new FileInputStream(file);
            DataInputStream in = new DataInputStream(fStream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));

            while ((line = br.readLine()) != null){
                st = new StringTokenizer(line, ";");
                set.add(Integer.parseInt(st.nextToken()));
            }
            in.close();
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return set;
    }
    
    public static ArrayList<Double> getHashes(String path){
        java.io.File file = new java.io.File(path);
        String line;
        StringTokenizer st;
        ArrayList<Double> set = new ArrayList<>();
        try {
            FileInputStream fStream = new FileInputStream(file);
            DataInputStream in = new DataInputStream(fStream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            
            while ((line = br.readLine()) != null){
                st = new StringTokenizer(line, ";");
                //st.nextToken();
                set.add(Double.parseDouble(st.nextToken()));
            }
            in.close();
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return set;
    }
    
    public static ArrayList<Solution> getSolutions(String path, Instance tsp, HashFunction h){
        java.io.File file = new java.io.File(path);
        String line,perm;
        int length=tsp.getDimension();
        int[] t;
        StringTokenizer st;
        ArrayList<Solution> set = new ArrayList<>();
        try {
            FileInputStream fStream = new FileInputStream(file);
            DataInputStream in = new DataInputStream(fStream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            
            while ((line = br.readLine()) != null){
                t=new int[length];
                st = new StringTokenizer(line, ";");
                st.nextToken();//st.nextToken();
                perm = st.nextToken();
                perm = perm.substring(1,perm.length()-1);
                String ss[]=perm.split(", ");
                for(int i=0;i<length;i++)
                    t[i]=Integer.parseInt(ss[i]);
                set.add(new Solution(tsp,t,h));
            }
            in.close();
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return set;
    }
    
    public static void instanceToFile(Instance tsp, String path, String name, String comment){
        BufferedWriter bw = null;
        FileWriter fw = null;
        try {
            java.io.File file = new java.io.File(path);
            if (!file.exists()) {
                file.createNewFile();
            }
            else{
                System.err.println("File already exists!");
                return;
            }
            fw = new FileWriter(file.getAbsoluteFile(), false);
            
            bw = new BufferedWriter(fw);
            bw.write("NAME : "+name);bw.newLine();
            bw.write("COMMENT : "+comment);bw.newLine();
            bw.write("TYPE : TSP");bw.newLine();
            bw.write("DIMENSION : "+tsp.getDimension());bw.newLine();
            bw.write("EDGE_WEIGHT_TYPE : "+tsp.getEdgeWeightType());bw.newLine();
            bw.write("NODE_COORD_SECTION");bw.newLine();
            for(int k=1;k<=tsp.getDimension();k++){
                bw.write(k+" "+tsp.getCities().get(k-1).getX()+" "+tsp.getCities().get(k-1).getY());bw.newLine();
            }
            bw.write("EOF");bw.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bw != null) {
                    bw.close();
                }
                if (fw != null) {
                    fw.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
    
}
