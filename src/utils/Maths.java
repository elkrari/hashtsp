package utils;

import java.util.ArrayList;
import java.util.Collections;

public class Maths {
	
	public static long mod(long a, long b){
		if(a==0 || b==0)
			return a+b;
		if(a<b)
			return b%a;
		return a%b;
	}
	
	public static int getPrime(int min) {
		int i = min+1;
		while(!isPrime(i++));
		return i-1;
		
	}
	
	public static boolean isPrime(int number) {
		for (int i = 2; i*i < number; i++) {
			if (number % i == 0) {
				return false;
			}
		}
		return true;
	}
	
	public static String randChar(int size){
		String out="";
		for(int i=0;i<size;i++)
			out = out+""+((char) (Math.random()*58 + 'A'));
		return out;
	}
	
	public static float average(int[] set){
		float sum=0;
		for(int n:set){
			sum+=n;
		}
		return sum/set.length;
	}
	public static float average(float[] set){
		float sum=0;
		for(float n:set){
			sum+=n;
		}
		return sum/set.length;
	}
	public static float average(double[] set){
		float sum=0;
		for(double n:set){
			sum+=n;
		}
		return sum/set.length;
	}
	public static float average(ArrayList<Integer> set){
		float sum=0;
		for(float n:set){
			sum+=n;
		}
		return sum/set.size();
	}
}
