package tsp;

import constructive.Random;
import hash.*;
import utils.*;

import java.util.Arrays;
import java.util.Objects;

public class Solution implements Comparable {
    private int[] tour;
    /*
    Position array size is n+1
    Nodes are indexed from 1 to n
    Position values are from 0 to n-1
     */
    private int[] pos;
    private int fitness;
    private long hash;
    
    private HashFunction hashFunction;
    
    /**
     * Construct a new Solution from the
     * given instance and tour
     * @param tsp A TSP instance
     * @param tour A permutation of cities
     */
    public Solution(Instance tsp, int[] tour, HashFunction h) {
        this.tour = tour;
        
        this.pos = new int[tsp.getDimension()+1];
        for(int x=0;x<tsp.getDimension();x++){
            pos[tour[x]]=x;
        }
        
        this.setFitness(tour,tsp);
        this.setHash(h.hash(this, tsp));
        this.hashFunction = h;
        
    }
    
    /**
     * Construct a new Solution from the
     * given instance and tour
     * @param tsp A TSP instance
     * @param tour A permutation of cities
     */
    public Solution(Instance tsp, int[] tour) {
        this.tour = tour;
        
        this.pos = new int[tsp.getDimension()+1];
        for(int x=0;x<tsp.getDimension();x++){
            pos[tour[x]]=x;
        }
        
        this.setFitness(tour,tsp);
        HashFunction h = new HashF1();
        this.setHash(h.hash(this,tsp));
        this.hashFunction = h;
    }

    /**
     * Construct a new Solution from the
     * given instance with a random tour
     * @param tsp A TSP instance
     */
    public Solution(Instance tsp, HashFunction h) {
        this.tour = new Random().newTour(tsp);

        this.pos = new int[tsp.getDimension()+1];
        for(int x=0;x<tsp.getDimension();x++){
            pos[tour[x]]=x;
        }

        this.setFitness(tour,tsp);
        this.setHash(h.hash(this,tsp));
        this.hashFunction = h;

    }
    
    /**
     * Construct a new Solution from the
     * given instance with a random tour
     * @param tsp A TSP instance
     */
    public Solution(Instance tsp) {
        this.tour = new Random().newTour(tsp);
        
        this.pos = new int[tsp.getDimension()+1];
        for(int x=0;x<tsp.getDimension();x++){
            pos[tour[x]]=x;
        }
        
        this.setFitness(tour,tsp);
        HashFunction h = new HashF1();
        this.setHash(h.hash(this,tsp));
        this.hashFunction = h;
        
    }

    public Solution(){ }

    public void setFitness(int[] tour, Instance tsp) {
        int f = 0;
        for (int x = 0; x < tsp.getDimension(); x++) {
            f += tsp.distance(tour[x],tour[(x + 1) % tsp.getDimension()]);//[tour[x]][tour[(x + 1) % tsp.getDimension()]];
        }
        this.fitness = f;
    }

    public void setFitness(int fitness) {
        this.fitness = fitness;
    }

    public int[] getTour() {
        return tour;
    }

    public int[] getPos() { return pos; }

    public int getFitness() {
        return fitness;
    }
    
    
    public long getHash() { return hash; }
    
    public void setHash(long hash) { this.hash = hash; }
    
    public HashFunction getHashFunction() {
        return hashFunction;
    }
    
    public void setHashFunction(HashFunction hashFunction) {
        this.hashFunction = hashFunction;
    }
    
    @Override
    public String toString() {
        return "fitness =" + fitness +
                ", hash =" + String.format("%d",hash) +
                ", tour =" + Arrays.toString(tour);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Solution solution = (Solution) o;
        return Similarity.areSimilar(this,solution);
        //return this.getHash() == solution.getHash();
        //return this.getFitness() == solution.getFitness();
    }

    @Override
    public int hashCode() {
        return Objects.hash(tour, pos, fitness);
    }

    public Solution copy(){
        Solution s = new Solution();
        s.tour = Arrays.copyOf(tour,tour.length);
        s.pos = Arrays.copyOf(pos,pos.length);
        s.fitness = fitness;
        s.hash = hash;
        s.hashFunction = hashFunction;

        return s;
    }
    
    @Override
    public int compareTo(Object o) {
        if (this == o) return 0;
        Solution solution = (Solution) o;
        return this.getFitness() - solution.getFitness();
    }
}
