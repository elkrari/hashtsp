package tsp;

import utils.Similarity;

public class Edge {
    int x;
    int y;

    public Edge(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Edge edge = (Edge) o;

        if ((x == edge.x && y == edge.y) || (x == edge.y && y == edge.x))
            return true;

        return false;
    }

    /*@Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }*/
}
