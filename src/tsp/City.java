package tsp;

import java.util.Scanner;

/**
 *
 * @author elkrari
 */
public class City {
    float x,y;
    
    public City(){}

    public City(float x, float y){
        this.x=x;
        this.y=y;
    }
    
    public boolean equal(City V){
        return (this.x==V.x && this.y==V.y);
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }
    
    public City setVille(Scanner br){
        br.nextInt();
        this.x=br.nextFloat();
        this.y=br.nextFloat();
        return this;
    }
    
    public City setVille(City V){
        this.x=V.x;
        this.y=V.y;
        return this;
    }
    
     
    public static int distEuc(City v1, City v2) {
        return (int) (Math.sqrt(Math.pow((v2.x - v1.x), 2) + Math.pow((v2.y - v1.y), 2)) + 0.5);
    }
    
    public static int distGeo(City v1, City v2) {
        double RRR = 6378.388;
        
        double q1 = Math.cos(degreeToRadian(v1.y) - degreeToRadian(v2.y));
        double q2 = Math.cos(degreeToRadian(v1.x) - degreeToRadian(v2.x));
        double q3 = Math.cos(degreeToRadian(v1.x) + degreeToRadian(v2.x));
        return (int) ( RRR * Math.acos( 0.5*((1.0+q1)*q2 - (1.0-q1)*q3) ) + 1.0);
    }
    
     private static double degreeToRadian(double coordinate){
    	int deg = (int) coordinate;
    	double min = coordinate - deg;
    	double rad = (Math.PI * (deg + (5 * min)/3)) / 180;
    	return rad;
    }
    
    @Override
    public String toString() {
        return "City{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
